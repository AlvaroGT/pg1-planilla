﻿using AccesoDatos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class Planilla : Form
    {
        public Planilla()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            PlanillaDAO ObjDesc = new PlanillaDAO();
            try
            {
                ObjDesc.Mes = textBox1.Text;
                ObjDesc.Parte = textBox2.Text;

                bool respuestaSQL = ObjDesc.InsertarDatos();
                if (respuestaSQL == true)
                {
                    MessageBox.Show("Los nuevos datos fueron insertados correctamente");
                    textBox1.Text = "";
                    textBox2.Text = "";
                }
                else
                {
                    MessageBox.Show(ObjDesc.Mensaje);
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show("Error!: " + Ex.Message + " " + ObjDesc.Mensaje);
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            PlanillaDAO ObjDesc = new PlanillaDAO();
            try
            {
                DataSet DatosDesc = ObjDesc.ConsultarDatos(int.Parse(textBox6.Text));
                int numregistros = DatosDesc.Tables["TablaConsultada"].Rows.Count;
                if (numregistros == 0)
                {
                    MessageBox.Show("No existe en la tabla este identificador");
                }
                else
                {
                    textBox7.Text = DatosDesc.Tables["TablaConsultada"].Rows[0]["mes"].ToString();
                    textBox8.Text = DatosDesc.Tables["TablaConsultada"].Rows[0]["parte"].ToString();
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show("Fatality!: " + Ex.Message + " " + ObjDesc.Mensaje);
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            PlanillaDAO ObjDesc = new PlanillaDAO();
            try
            {
                ObjDesc.Identificador = int.Parse(textBox6.Text);
                ObjDesc.Mes = textBox7.Text;
                ObjDesc.Parte = textBox8.Text;

                bool respuestaSQL = ObjDesc.ActualizarDatos();
                if (respuestaSQL == true)
                {
                    MessageBox.Show("Los datos fueron actualizados correctamente");
                    textBox6.Text = "";
                    textBox7.Text = "";
                    textBox8.Text = "";
                }
                else
                {
                    MessageBox.Show(ObjDesc.Mensaje);
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show("Fatality!: " + Ex.Message + " " + ObjDesc.Mensaje);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            PlanillaDAO ObjDesc = new PlanillaDAO();
            try
            {
                bool respuestaSQL = ObjDesc.EliminarDatos(textBox6.Text);
                if (respuestaSQL == true)
                {
                    MessageBox.Show("Los datos fueron Eliminados correctamente");
                    textBox6.Text = "";
                    textBox7.Text = "";
                    textBox8.Text = "";
                }
                else
                {
                    MessageBox.Show(ObjDesc.Mensaje);
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show("Fatality!: " + Ex.Message + " " + ObjDesc.Mensaje);
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void Descuento_Load(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {

        }

        private void button5_Click_1(object sender, EventArgs e)
        {
            PlanillaDAO ObjDesc = new PlanillaDAO();
            try
            {
                if (string.IsNullOrEmpty(textBox11.Text ?? string.Empty))
                {
                    MessageBox.Show("Ingrese el id por favor");
                    return;
                }
                int iddesc = int.Parse(textBox11.Text);
                DataSet DatosDesc = ObjDesc.ConsultarDatos(iddesc);
                dataGridView1.DataSource = DatosDesc.Tables["TablaConsultada"].DefaultView;
            }
            catch (Exception Ex)
            {
                MessageBox.Show("Fatality: " + Ex.Message + " " + ObjDesc.Mensaje);
            }
        }

        private void button6_Click_1(object sender, EventArgs e)
        {
            PlanillaDAO ObjDesc = new PlanillaDAO();
            try
            {
                DataSet DatosDesc = ObjDesc.ConsultarTodosDatos();
                dataGridView1.DataSource = DatosDesc.Tables["TablaConsultada"].DefaultView;
            }
            catch (Exception Ex)
            {
                MessageBox.Show("Fatality: " + Ex.Message + " " + ObjDesc.Mensaje);
            }
        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void label14_Click(object sender, EventArgs e)
        {

        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void button7_Click(object sender, EventArgs e)
        {
            Empleados anterior = new Empleados();
            anterior.Show();
            this.Hide();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            DetaPlanilla siguiente = new DetaPlanilla();
            siguiente.Show();
            this.Hide();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            Menu menu = new Menu();
            menu.Show();
            this.Hide();
        }
    }
}
