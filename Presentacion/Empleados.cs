﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AccesoDatos;

namespace Presentacion
{
    public partial class Empleados : Form
    {
        public Empleados()
        {
            Select select = new Select();
            InitializeComponent();
            cbtp.DataSource = select.ListaTipopagos().Tables["TablaConsultada"];
            cbtp.DisplayMember = "tipopago";
            cbtp.ValueMember = "idtipopago";

            cbtp2.DataSource = select.ListaTipopagos().Tables["TablaConsultada"];
            cbtp2.DisplayMember = "tipopago";
            cbtp2.ValueMember = "idtipopago";

            cbd.DataSource = select.ListaDepartamento().Tables["TablaConsultada"];
            cbd.DisplayMember = "departamento";
            cbd.ValueMember = "iddepartamento";

            cbd2.DataSource = select.ListaDepartamento().Tables["TablaConsultada"];
            cbd2.DisplayMember = "departamento";
            cbd2.ValueMember = "iddepartamento";
        }



        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void textBox13_TextChanged(object sender, EventArgs e)
        {

        }

        private void label25_Click(object sender, EventArgs e)
        {

        }

        private void textBox25_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            EmpleadoDAO empleado = new EmpleadoDAO();
            try
            {
                empleado.codempleado = int.Parse(txtcodigo.Text);
                empleado.nombre = txtnombre.Text;
                empleado.apellido = txtapellido.Text;
                empleado.direccion = txtdireccion.Text;
                empleado.telefono = txttelefono.Text;
                empleado.dui = txtdui.Text;
                empleado.nit = txtnit.Text;
                empleado.enfermedad = txtenfermedad.Text;
                empleado.fechaingreso = dateTimePicker1.Value.ToString("dd'-'MM'-'yyyy", System.Globalization.CultureInfo.InvariantCulture);
                empleado.sueldo = double.Parse(txtsueldo.Text);
                empleado.idtipopago = int.Parse(cbtp.SelectedValue.ToString());
                empleado.iddepartamento = int.Parse(cbd.SelectedValue.ToString());
                bool respuestaSQL = empleado.insertar();
                if (respuestaSQL == true)
                {
                    MessageBox.Show("NUEVO EMPLEADO AGREGADO");
                    txtcodigo.Text = "";
                    txtnombre.Text = "";
                    txtapellido.Text = "";
                    txtdireccion.Text = "";
                    txtdui.Text = "";
                    txtnit.Text = "";
                    txtenfermedad.Text = "";
                    txttelefono.Text = "";
                    txtsueldo.Text = "";
                }
                else {
                    MessageBox.Show("Nop no se pudo");
                }
                
            }
            catch (Exception)
            {
                MessageBox.Show("nop nisiquiera pudo insertar");
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            EmpleadoDAO empleados = new EmpleadoDAO();

            try
            {
                DataSet Lista = empleados.ListaMostrar();
                dataGridView1.DataSource = Lista.Tables["TablaConsultada"].DefaultView;
            }
            catch (Exception Ex)
            {
                Console.WriteLine("ERROR:" + Ex.Message);
            }
        }

        private void Empleados_Load(object sender, EventArgs e)
        {
            EmpleadoDAO empleados = new EmpleadoDAO();
            dateTimePicker1.Format = DateTimePickerFormat.Custom;
            dateTimePicker1.CustomFormat = "dd-MM-yyyy";
            try
            {
                DataSet Lista = empleados.ListaMostrar();
                dataGridView1.DataSource = Lista.Tables["TablaConsultada"].DefaultView;
            }
            catch (Exception Ex)
            {
                Console.WriteLine("ERROR:" + Ex.Message);
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {

            if (string.IsNullOrEmpty(idmostrar.Text ?? string.Empty))
            {
                MessageBox.Show("Ingrese el id del cliente por favor");
                return;
            }
            EmpleadoDAO empleados = new EmpleadoDAO();
            int id = int.Parse(idmostrar.Text);
            DataSet Datos = empleados.Consultar(id);
            dataGridView1.DataSource = Datos.Tables["TablaConsultada"].DefaultView;
        }

        private void txtdepartamento_TextChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {/*
            EmpleadoDAO empleados = new EmpleadoDAO();
            try
            {
                DataSet empleado = empleados.Consultar(int.Parse(Eid.Text));
                int numretros = empleado.Tables["TablaConsultada"].Rows.Count;
                if (numretros == 0)
                {
                    MessageBox.Show("No existe en la tabla Descuentos este identificador");
                }
                else {
                    Eid.Text = empleado.Tables["TablaConsultada"].Rows[0]["idempleado"].ToString();
                    Ecodigo.Text = empleado.Tables["TablaConsultada"].Rows[0]["codempleado"].ToString();
                    Enombre.Text = empleado.Tables["TablaConsultada"].Rows[0]["nombre"].ToString();
                    Eapellido.Text = empleado.Tables["TablaConsultada"].Rows[0]["apellido"].ToString();
                    Edireccion.Text = empleado.Tables["TablaConsultada"].Rows[0]["direccion"].ToString();
                    Etelefono.Text = empleado.Tables["TablaConsultada"].Rows[0]["telefono"].ToString();
                    Edui.Text = empleado.Tables["TablaConsultada"].Rows[0]["dui"].ToString();
                    Enit.Text = empleado.Tables["TablaConsultada"].Rows[0]["nit"].ToString();
                    Eenfermedad.Text = empleado.Tables["TablaConsultada"].Rows[0]["enfermedad"].ToString();
                    dateTimePicker2.Value = Convert.ToDateTime(empleado.Tables["TablaConsultada"].Rows[0]["fechaingreso"]);
                    Esaldo.Text = empleado.Tables["TablaConsultada"].Rows[0]["sueldo"].ToString();
                    cbd2.SelectedValue = empleado.Tables["TablaConsultada"].Rows[0]["iddepartamento"].ToString();
                    cbd2.SelectedValue = empleado.Tables["TablaConsultada"].Rows[0]["idtipopago"].ToString();

                }
            }
            catch (Exception)
            {
                MessageBox.Show("ERROR");
            }*/

        }

        private void button2_Click(object sender, EventArgs e)
        {/*
            EmpleadoDAO empleado = new EmpleadoDAO();
            try
            {
                empleado.idempleado     = int.Parse(Eid.Text);
                empleado.codempleado    = int.Parse(Ecodigo.Text);
                empleado.nombre         = Enombre.Text;
                empleado.apellido       = Eapellido.Text;
                empleado.direccion      = Edireccion.Text;
                empleado.telefono       = Etelefono.Text;
                empleado.dui            = Edui.Text;
                empleado.nit            = Enit.Text;
                empleado.enfermedad     = Eenfermedad.Text;
                empleado.fechaingreso   = dateTimePicker2.Value.ToString("dd'-'MM'-'yyyy", System.Globalization.CultureInfo.InvariantCulture);
                empleado.sueldo         = double.Parse(Esaldo.Text);
                empleado.idtipopago     = int.Parse(cbtp.SelectedValue.ToString());
                empleado.iddepartamento = int.Parse(cbd2.SelectedValue.ToString());
                



                bool respuestaSQL = empleado.Actualizar();
                if (respuestaSQL == true)
                {
                    MessageBox.Show("Los datos fueron actualizado correctamente");
                    Eid.Text = "";
                    Ecodigo.Text = "";
                    Enombre.Text = "";
                    Eapellido.Text = "";
                    Edireccion.Text = "";
                    Etelefono.Text = "";
                    Edui.Text = "";
                    Enit.Text = "";
                    Eenfermedad.Text = "";
                    Esaldo.Text = "";
                }
                else {
                    MessageBox.Show("Nop no se pudo");
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show("Fatality!: " + Ex.Message);
            }*/
        }

        

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void iddepartamento_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            EmpleadoDAO empleado = new EmpleadoDAO();
            try
            {
                empleado.idempleado = int.Parse(idmod.Text);
                empleado.codempleado = int.Parse(cmod.Text);
                empleado.nombre = nmod.Text;
                empleado.apellido = amod.Text;
                empleado.direccion = dmod.Text;
                empleado.telefono = tmod.Text;
                empleado.dui = duimod.Text;
                empleado.nit = nitmod.Text;
                empleado.enfermedad = emod.Text;
                empleado.fechaingreso = dateTimePicker2.Value.ToString("dd'-'MM'-'yyyy", System.Globalization.CultureInfo.InvariantCulture);
                empleado.sueldo = double.Parse(smod.Text);
                empleado.idtipopago = int.Parse(cbtp2.SelectedValue.ToString());
                empleado.iddepartamento = int.Parse(cbd2.SelectedValue.ToString());
                if (empleado.Actualizar())
                {
                    MessageBox.Show("EMPLEADO ACTUALIZADO CON EXITO");
                    idmod.Text = "";
                    cmod.Text = "";
                    nmod.Text = "";
                    amod.Text = "";
                    dmod.Text = "";
                    tmod.Text = "";
                    duimod.Text = "";
                    nitmod.Text = "";
                    emod.Text = "";
                    smod.Text = "";
                }
                else {
                    MessageBox.Show("No se pudo modificar el Empleado");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR: " + ex.Message);
            }
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            EmpleadoDAO empleados = new EmpleadoDAO();
            try
            {
                DataSet empleado = empleados.Consultar(int.Parse(idmod.Text));
                int numretros = empleado.Tables["TablaConsultada"].Rows.Count;
                if (numretros == 0)
                {
                    MessageBox.Show("No existe en la tabla Descuentos este identificador");
                }
                else
                {
                    idmod.Text = empleado.Tables["TablaConsultada"].Rows[0]["idempleado"].ToString();
                    cmod.Text = empleado.Tables["TablaConsultada"].Rows[0]["codempleado"].ToString();
                    nmod.Text = empleado.Tables["TablaConsultada"].Rows[0]["nombre"].ToString();
                    amod.Text = empleado.Tables["TablaConsultada"].Rows[0]["apellido"].ToString();
                    dmod.Text = empleado.Tables["TablaConsultada"].Rows[0]["direccion"].ToString();
                    tmod.Text = empleado.Tables["TablaConsultada"].Rows[0]["telefono"].ToString();
                    duimod.Text = empleado.Tables["TablaConsultada"].Rows[0]["dui"].ToString();
                    nitmod.Text = empleado.Tables["TablaConsultada"].Rows[0]["nit"].ToString();
                    emod.Text = empleado.Tables["TablaConsultada"].Rows[0]["enfermedad"].ToString();
                    dateTimePicker2.Value = Convert.ToDateTime(empleado.Tables["TablaConsultada"].Rows[0]["fechaingreso"]);
                    smod.Text = empleado.Tables["TablaConsultada"].Rows[0]["sueldo"].ToString();
                    cbd2.SelectedValue = empleado.Tables["TablaConsultada"].Rows[0]["iddepartamento"].ToString();
                    cbd2.SelectedValue = empleado.Tables["TablaConsultada"].Rows[0]["idtipopago"].ToString();

                }
            }
            catch (Exception)
            {
                MessageBox.Show("ERROR");
            }

        }

        private void button4_Click(object sender, EventArgs e)
        {
            EmpleadoDAO empleado = new EmpleadoDAO();
            try
            {
                empleado.idempleado = int.Parse(idmod.Text);
                empleado.codempleado = int.Parse(cmod.Text);
                empleado.nombre = nmod.Text;
                empleado.apellido = amod.Text;
                empleado.direccion = dmod.Text;
                empleado.telefono = tmod.Text;
                empleado.dui = duimod.Text;
                empleado.nit = nitmod.Text;
                empleado.enfermedad = emod.Text;
                empleado.fechaingreso = dateTimePicker2.Value.ToString("dd'-'MM'-'yyyy", System.Globalization.CultureInfo.InvariantCulture);
                empleado.sueldo = double.Parse(smod.Text);
                empleado.idtipopago = int.Parse(cbtp2.SelectedValue.ToString());
                empleado.iddepartamento = int.Parse(cbd2.SelectedValue.ToString());
                if (empleado.Eliminar(int.Parse(idmod.Text)))
                {
                    MessageBox.Show("EMPLEADO ELIMINADO CON EXITO");
                    idmod.Text = "";
                    cmod.Text = "";
                    nmod.Text = "";
                    amod.Text = "";
                    dmod.Text = "";
                    tmod.Text = "";
                    duimod.Text = "";
                    nitmod.Text = "";
                    emod.Text = "";
                    smod.Text = "";
                }
                else
                {
                    MessageBox.Show("No se pudo modificar el Empleado");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR: " + ex.Message);
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            Menu menu = new Menu();
            menu.Show();
            this.Hide();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Descuento anterior = new Descuento();
            anterior.Show();
            this.Hide();
        }

        private void idmostrar_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 43) || (e.KeyChar >= 58 && e.KeyChar <= 255) || e.KeyChar == 45 || e.KeyChar == 47)
            {
                MessageBox.Show("Solo números, punto, y coma", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void idmod_TextChanged(object sender, EventArgs e)
        {

        }

        private void button8_Click(object sender, EventArgs e)
        {
            Planilla plan = new Planilla();
            plan.Show();
            this.Hide();
        }
    }
    
}
