﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Donimio;

namespace Presentacion
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if (Usuario.Text != "" && Pass.Text != "")
            {
                UsuarioModelo user = new UsuarioModelo();
                var validaLogin = user.LoginUser(Usuario.Text, Pass.Text );
                if (validaLogin == true)
                {
                    Menu Menu = new Menu();
                    Menu.Show();
                    this.Hide();
                }
                else {
                    mensaje.Text = "Credenciales no válidas";
                }
            }
            else {
                mensaje.Text = "Complete todos los campos";
            }
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void Usuario_TextChanged(object sender, EventArgs e)
        {

        }

        private void Login_Load(object sender, EventArgs e)
        {

        }
    }
}
