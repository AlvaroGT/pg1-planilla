﻿using AccesoDatos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class DetaPlanilla : Form
    {
        public DetaPlanilla()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DetPlanillaDAO ObjDatos = new DetPlanillaDAO();
            try
            {
                ObjDatos.Idplanilla = int.Parse(textBox1.Text);
                ObjDatos.Idempleados = int.Parse(textBox2.Text);
                ObjDatos.Iddescuentos = int.Parse(textBox3.Text);
                ObjDatos.Horasextradiu = int.Parse(textBox4.Text);
                ObjDatos.Horasextranoc = int.Parse(textBox5.Text);
                ObjDatos.Bonos = textBox6.Text;
                ObjDatos.Horasdiu = int.Parse(textBox7.Text);
                ObjDatos.Horasnoc = int.Parse(textBox8.Text);
                ObjDatos.Isss = textBox9.Text;
                ObjDatos.Afp = textBox10.Text;
                ObjDatos.Estadorenta = textBox11.Text;
                ObjDatos.Renta = textBox12.Text;
                ObjDatos.Salarioneto = textBox13.Text;

                bool respuestaSQL = ObjDatos.InsertarDatos();
                if (respuestaSQL == true)
                {
                    MessageBox.Show("Los datos nuevos fueron insertados correctamente");
                    textBox1.Text = "";
                    textBox2.Text = "";
                    textBox3.Text = "";
                    textBox4.Text = "";
                    textBox5.Text = "";
                    textBox6.Text = "";
                    textBox7.Text = "";
                    textBox8.Text = "";
                    textBox9.Text = "";
                    textBox10.Text = "";
                    textBox11.Text = "";
                    textBox12.Text = "";
                    textBox13.Text = "";
                }
                else
                {
                    MessageBox.Show(ObjDatos.Mensaje);
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show("Error!: " + Ex.Message + " " + ObjDatos.Mensaje);
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            DetPlanillaDAO ObjDatos = new DetPlanillaDAO();
            try
            {
                DataSet DatosDesc = ObjDatos.ConsultarDatos(int.Parse(textBox14.Text));
                int numregistros = DatosDesc.Tables["TablaConsultada"].Rows.Count;
                if (numregistros == 0)
                {
                    MessageBox.Show("No existe en la tabla Descuentos este identificador");
                }
                else
                {
                    textBox15.Text = DatosDesc.Tables["TablaConsultada"].Rows[0]["idplanilla"].ToString();
                    textBox16.Text = DatosDesc.Tables["TablaConsultada"].Rows[0]["idempleados"].ToString();
                    textBox17.Text = DatosDesc.Tables["TablaConsultada"].Rows[0]["iddescuentos"].ToString();
                    textBox18.Text = DatosDesc.Tables["TablaConsultada"].Rows[0]["horasextradiu"].ToString();
                    textBox19.Text = DatosDesc.Tables["TablaConsultada"].Rows[0]["horasextranoc"].ToString();
                    textBox20.Text = DatosDesc.Tables["TablaConsultada"].Rows[0]["bonos"].ToString();
                    textBox21.Text = DatosDesc.Tables["TablaConsultada"].Rows[0]["horasdiu"].ToString();
                    textBox22.Text = DatosDesc.Tables["TablaConsultada"].Rows[0]["horasnoc"].ToString();
                    textBox23.Text = DatosDesc.Tables["TablaConsultada"].Rows[0]["isss"].ToString();
                    textBox24.Text = DatosDesc.Tables["TablaConsultada"].Rows[0]["afp"].ToString();
                    textBox25.Text = DatosDesc.Tables["TablaConsultada"].Rows[0]["estadorenta"].ToString();
                    textBox26.Text = DatosDesc.Tables["TablaConsultada"].Rows[0]["renta"].ToString();
                    textBox27.Text = DatosDesc.Tables["TablaConsultada"].Rows[0]["salarioneto"].ToString();

                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show("Fatality!: " + Ex.Message + " " + ObjDatos.Mensaje);
            }
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DetPlanillaDAO ObjDatos = new DetPlanillaDAO();
            try
            {
                ObjDatos.Identificador = int.Parse(textBox14.Text);
                ObjDatos.Idplanilla = int.Parse(textBox15.Text);
                ObjDatos.Idempleados = int.Parse(textBox16.Text);
                ObjDatos.Iddescuentos = int.Parse(textBox17.Text);
                ObjDatos.Horasextradiu = int.Parse(textBox18.Text);
                ObjDatos.Horasextranoc = int.Parse(textBox19.Text);
                ObjDatos.Bonos = textBox20.Text;
                ObjDatos.Horasdiu = int.Parse(textBox21.Text);
                ObjDatos.Horasnoc = int.Parse(textBox22.Text);
                ObjDatos.Isss = textBox23.Text;
                ObjDatos.Afp = textBox24.Text;
                ObjDatos.Estadorenta = textBox25.Text;
                ObjDatos.Renta = textBox26.Text;
                ObjDatos.Salarioneto = textBox27.Text;

                bool respuestaSQL = ObjDatos.ActualizarDatos();
                if (respuestaSQL == true)
                {
                    MessageBox.Show("Los datos fueron actualizados correctamente");
                    textBox14.Text = "";
                    textBox15.Text = "";
                    textBox16.Text = "";
                    textBox17.Text = "";
                    textBox18.Text = "";
                    textBox19.Text = "";
                    textBox20.Text = "";
                    textBox21.Text = "";
                    textBox22.Text = "";
                    textBox23.Text = "";
                    textBox24.Text = "";
                    textBox25.Text = "";
                    textBox26.Text = "";
                    textBox27.Text = "";
                }
                else
                {
                    MessageBox.Show(ObjDatos.Mensaje);
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show("Fatality!: " + Ex.Message + " " + ObjDatos.Mensaje);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            DetPlanillaDAO ObjDatos = new DetPlanillaDAO();
            try
            {
                bool respuestaSQL = ObjDatos.EliminarDatos(textBox14.Text);
                if (respuestaSQL == true)
                {
                    MessageBox.Show("Los datos fueron Eliminados correctamente");
                    textBox14.Text = "";
                    textBox15.Text = "";
                    textBox16.Text = "";
                    textBox17.Text = "";
                    textBox18.Text = "";
                    textBox19.Text = "";
                    textBox20.Text = "";
                    textBox21.Text = "";
                    textBox22.Text = "";
                    textBox23.Text = "";
                    textBox24.Text = "";
                    textBox25.Text = "";
                    textBox26.Text = "";
                    textBox27.Text = "";
                }
                else
                {
                    MessageBox.Show(ObjDatos.Mensaje);
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show("Fatality!: " + Ex.Message + " " + ObjDatos.Mensaje);
            }
        }

        

        private void button5_Click_1(object sender, EventArgs e)
        {
            DetPlanillaDAO ObjDatos = new DetPlanillaDAO();
            try
            {
                if (string.IsNullOrEmpty(textBox28.Text ?? string.Empty))
                {
                    MessageBox.Show("Ingrese el id por favor");
                    return;
                }
                int iddesc = int.Parse(textBox28.Text);
                DataSet DatosDesc = ObjDatos.ConsultarDatos(iddesc);
                dataGridView1.DataSource = DatosDesc.Tables["TablaConsultada"].DefaultView;
            }
            catch (Exception Ex)
            {
                MessageBox.Show("Fatality: " + Ex.Message + " " + ObjDatos.Mensaje);
            }
        }

        private void button6_Click_1(object sender, EventArgs e)
        {
            DetPlanillaDAO ObjDatos = new DetPlanillaDAO();
            try
            {
                DataSet DatosDesc = ObjDatos.ConsultarTodosDatos();
                dataGridView1.DataSource = DatosDesc.Tables["TablaConsultada"].DefaultView;
            }
            catch (Exception Ex)
            {
                MessageBox.Show("Fatality: " + Ex.Message + " " + ObjDatos.Mensaje);
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void Descuento_Load(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 43) || (e.KeyChar >= 59 && e.KeyChar <= 255) || e.KeyChar == 45 || e.KeyChar == 47)
            {
                MessageBox.Show("Solo números, punto, y coma", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }
        
        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 43) || (e.KeyChar >= 59 && e.KeyChar <= 255) || e.KeyChar == 45 || e.KeyChar == 47)
            {
                MessageBox.Show("Solo números, punto, y coma", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 43) || (e.KeyChar >= 59 && e.KeyChar <= 255) || e.KeyChar == 45 || e.KeyChar == 47)
            {
                MessageBox.Show("Solo números, punto, y coma", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void textBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 43) || (e.KeyChar >= 59 && e.KeyChar <= 255) || e.KeyChar == 45 || e.KeyChar == 47)
            {
                MessageBox.Show("Solo números, punto, y coma", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void textBox5_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 43) || (e.KeyChar >= 59 && e.KeyChar <= 255) || e.KeyChar == 45 || e.KeyChar == 47)
            {
                MessageBox.Show("Solo números, punto, y coma", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void textBox7_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void textBox7_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 43) || (e.KeyChar >= 59 && e.KeyChar <= 255) || e.KeyChar == 45 || e.KeyChar == 47)
            {
                MessageBox.Show("Solo números, punto, y coma", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void textBox8_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 43) || (e.KeyChar >= 59 && e.KeyChar <= 255) || e.KeyChar == 45 || e.KeyChar == 47)
            {
                MessageBox.Show("Solo números, punto, y coma", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void textBox6_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 43) || (e.KeyChar >= 59 && e.KeyChar <= 255) || e.KeyChar == 45 || e.KeyChar == 47)
            {
                MessageBox.Show("Solo números, punto, y coma", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void textBox9_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 43) || (e.KeyChar >= 59 && e.KeyChar <= 255) || e.KeyChar == 45 || e.KeyChar == 47)
            {
                MessageBox.Show("Solo números, punto, y coma", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void textBox10_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 43) || (e.KeyChar >= 59 && e.KeyChar <= 255) || e.KeyChar == 45 || e.KeyChar == 47)
            {
                MessageBox.Show("Solo números, punto, y coma", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void textBox12_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 43) || (e.KeyChar >= 59 && e.KeyChar <= 255) || e.KeyChar == 45 || e.KeyChar == 47)
            {
                MessageBox.Show("Solo números, punto, y coma", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void textBox13_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 43) || (e.KeyChar >= 59 && e.KeyChar <= 255) || e.KeyChar == 45 || e.KeyChar == 47)
            {
                MessageBox.Show("Solo números, punto, y coma", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void textBox14_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 43) || (e.KeyChar >= 59 && e.KeyChar <= 255) || e.KeyChar == 45 || e.KeyChar == 47)
            {
                MessageBox.Show("Solo números, punto, y coma", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void textBox15_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 43) || (e.KeyChar >= 59 && e.KeyChar <= 255) || e.KeyChar == 45 || e.KeyChar == 47)
            {
                MessageBox.Show("Solo números, punto, y coma", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void textBox16_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 43) || (e.KeyChar >= 59 && e.KeyChar <= 255) || e.KeyChar == 45 || e.KeyChar == 47)
            {
                MessageBox.Show("Solo números, punto, y coma", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void textBox17_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 43) || (e.KeyChar >= 59 && e.KeyChar <= 255) || e.KeyChar == 45 || e.KeyChar == 47)
            {
                MessageBox.Show("Solo números, punto, y coma", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void textBox18_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 43) || (e.KeyChar >= 59 && e.KeyChar <= 255) || e.KeyChar == 45 || e.KeyChar == 47)
            {
                MessageBox.Show("Solo números, punto, y coma", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void textBox19_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 43) || (e.KeyChar >= 59 && e.KeyChar <= 255) || e.KeyChar == 45 || e.KeyChar == 47)
            {
                MessageBox.Show("Solo números, punto, y coma", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void textBox20_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 43) || (e.KeyChar >= 59 && e.KeyChar <= 255) || e.KeyChar == 45 || e.KeyChar == 47)
            {
                MessageBox.Show("Solo números, punto, y coma", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void textBox21_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 43) || (e.KeyChar >= 59 && e.KeyChar <= 255) || e.KeyChar == 45 || e.KeyChar == 47)
            {
                MessageBox.Show("Solo números, punto, y coma", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void textBox22_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 43) || (e.KeyChar >= 59 && e.KeyChar <= 255) || e.KeyChar == 45 || e.KeyChar == 47)
            {
                MessageBox.Show("Solo números, punto, y coma", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void textBox23_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 43) || (e.KeyChar >= 59 && e.KeyChar <= 255) || e.KeyChar == 45 || e.KeyChar == 47)
            {
                MessageBox.Show("Solo números, punto, y coma", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void textBox24_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 43) || (e.KeyChar >= 59 && e.KeyChar <= 255) || e.KeyChar == 45 || e.KeyChar == 47)
            {
                MessageBox.Show("Solo números, punto, y coma", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void textBox26_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 43) || (e.KeyChar >= 59 && e.KeyChar <= 255) || e.KeyChar == 45 || e.KeyChar == 47)
            {
                MessageBox.Show("Solo números, punto, y coma", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void textBox27_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 43) || (e.KeyChar >= 59 && e.KeyChar <= 255) || e.KeyChar == 45 || e.KeyChar == 47)
            {
                MessageBox.Show("Solo números, punto, y coma", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void textBox16_TextChanged(object sender, EventArgs e)
        {

        }

        private void button9_Click(object sender, EventArgs e)
        {
            Menu menu = new Menu();
            menu.Show();
            this.Hide();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Planilla plan = new Planilla();
            plan.Show();
            this.Hide();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Usuario user = new Usuario();
            user.Show();
            this.Hide();
        }
    }
}
