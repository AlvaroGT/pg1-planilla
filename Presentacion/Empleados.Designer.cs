﻿namespace Presentacion
{
    partial class Empleados
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Empleados));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.Mostrar = new System.Windows.Forms.TabPage();
            this.label28 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.idmostrar = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.cbd = new System.Windows.Forms.ComboBox();
            this.cbtp = new System.Windows.Forms.ComboBox();
            this.label29 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtsueldo = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtenfermedad = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtnit = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtdui = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txttelefono = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtdireccion = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtcodigo = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtapellido = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtnombre = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.smod = new System.Windows.Forms.TextBox();
            this.tmod = new System.Windows.Forms.TextBox();
            this.emod = new System.Windows.Forms.TextBox();
            this.duimod = new System.Windows.Forms.TextBox();
            this.nitmod = new System.Windows.Forms.TextBox();
            this.dmod = new System.Windows.Forms.TextBox();
            this.amod = new System.Windows.Forms.TextBox();
            this.nmod = new System.Windows.Forms.TextBox();
            this.cmod = new System.Windows.Forms.TextBox();
            this.idmod = new System.Windows.Forms.TextBox();
            this.cbd2 = new System.Windows.Forms.ComboBox();
            this.cbtp2 = new System.Windows.Forms.ComboBox();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.label30 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.label25 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.Esaldo = new System.Windows.Forms.TextBox();
            this.Eenfermedad = new System.Windows.Forms.TextBox();
            this.Enit = new System.Windows.Forms.TextBox();
            this.Edui = new System.Windows.Forms.TextBox();
            this.Etelefono = new System.Windows.Forms.TextBox();
            this.Edireccion = new System.Windows.Forms.TextBox();
            this.Ecodigo = new System.Windows.Forms.TextBox();
            this.Eapellido = new System.Windows.Forms.TextBox();
            this.Enombre = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.button9 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.Mostrar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.Mostrar);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 86);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1068, 504);
            this.tabControl1.TabIndex = 0;
            // 
            // Mostrar
            // 
            this.Mostrar.Controls.Add(this.label28);
            this.Mostrar.Controls.Add(this.pictureBox1);
            this.Mostrar.Controls.Add(this.button6);
            this.Mostrar.Controls.Add(this.button5);
            this.Mostrar.Controls.Add(this.dataGridView1);
            this.Mostrar.Controls.Add(this.idmostrar);
            this.Mostrar.Controls.Add(this.label26);
            this.Mostrar.Location = new System.Drawing.Point(4, 22);
            this.Mostrar.Name = "Mostrar";
            this.Mostrar.Padding = new System.Windows.Forms.Padding(3);
            this.Mostrar.Size = new System.Drawing.Size(1060, 478);
            this.Mostrar.TabIndex = 0;
            this.Mostrar.Text = "Consultar";
            this.Mostrar.UseVisualStyleBackColor = true;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Roboto Medium", 24F, System.Drawing.FontStyle.Bold);
            this.label28.Location = new System.Drawing.Point(16, 30);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(341, 38);
            this.label28.TabIndex = 16;
            this.label28.Text = "Consultar empleados";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Presentacion.Properties.Resources.Supermarket_workers_amico;
            this.pictureBox1.Location = new System.Drawing.Point(596, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(464, 475);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 13;
            this.pictureBox1.TabStop = false;
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(57)))), ((int)(((byte)(67)))), ((int)(((byte)(183)))));
            this.button6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.Font = new System.Drawing.Font("Roboto", 10F);
            this.button6.ForeColor = System.Drawing.Color.White;
            this.button6.Location = new System.Drawing.Point(385, 100);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(92, 28);
            this.button6.TabIndex = 3;
            this.button6.Text = "Consultar";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(29)))), ((int)(((byte)(74)))));
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Roboto", 10F);
            this.button5.ForeColor = System.Drawing.Color.White;
            this.button5.Location = new System.Drawing.Point(483, 100);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(86, 28);
            this.button5.TabIndex = 2;
            this.button5.Text = "Ver todos";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(23, 134);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(546, 325);
            this.dataGridView1.TabIndex = 2;
            // 
            // idmostrar
            // 
            this.idmostrar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.idmostrar.Location = new System.Drawing.Point(23, 100);
            this.idmostrar.Multiline = true;
            this.idmostrar.Name = "idmostrar";
            this.idmostrar.Size = new System.Drawing.Size(356, 28);
            this.idmostrar.TabIndex = 1;
            this.idmostrar.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.idmostrar_KeyPress);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Roboto", 9.75F);
            this.label26.Location = new System.Drawing.Point(20, 82);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(79, 15);
            this.label26.TabIndex = 0;
            this.label26.Text = "ID empleado";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.dateTimePicker1);
            this.tabPage3.Controls.Add(this.cbd);
            this.tabPage3.Controls.Add(this.cbtp);
            this.tabPage3.Controls.Add(this.label29);
            this.tabPage3.Controls.Add(this.button1);
            this.tabPage3.Controls.Add(this.label12);
            this.tabPage3.Controls.Add(this.label13);
            this.tabPage3.Controls.Add(this.txtsueldo);
            this.tabPage3.Controls.Add(this.label11);
            this.tabPage3.Controls.Add(this.label8);
            this.tabPage3.Controls.Add(this.txtenfermedad);
            this.tabPage3.Controls.Add(this.label9);
            this.tabPage3.Controls.Add(this.txtnit);
            this.tabPage3.Controls.Add(this.label6);
            this.tabPage3.Controls.Add(this.txtdui);
            this.tabPage3.Controls.Add(this.label7);
            this.tabPage3.Controls.Add(this.txttelefono);
            this.tabPage3.Controls.Add(this.label4);
            this.tabPage3.Controls.Add(this.txtdireccion);
            this.tabPage3.Controls.Add(this.label5);
            this.tabPage3.Controls.Add(this.txtcodigo);
            this.tabPage3.Controls.Add(this.label3);
            this.tabPage3.Controls.Add(this.txtapellido);
            this.tabPage3.Controls.Add(this.label2);
            this.tabPage3.Controls.Add(this.txtnombre);
            this.tabPage3.Controls.Add(this.label1);
            this.tabPage3.Controls.Add(this.pictureBox2);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1060, 478);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Crear";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(774, 297);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(244, 20);
            this.dateTimePicker1.TabIndex = 31;
            // 
            // cbd
            // 
            this.cbd.FormattingEnabled = true;
            this.cbd.Location = new System.Drawing.Point(774, 383);
            this.cbd.Name = "cbd";
            this.cbd.Size = new System.Drawing.Size(244, 21);
            this.cbd.TabIndex = 30;
            this.cbd.SelectedIndexChanged += new System.EventHandler(this.iddepartamento_SelectedIndexChanged);
            // 
            // cbtp
            // 
            this.cbtp.FormattingEnabled = true;
            this.cbtp.Location = new System.Drawing.Point(518, 383);
            this.cbtp.Name = "cbtp";
            this.cbtp.Size = new System.Drawing.Size(227, 21);
            this.cbtp.TabIndex = 29;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Roboto Medium", 24F, System.Drawing.FontStyle.Bold);
            this.label29.Location = new System.Drawing.Point(509, 49);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(313, 38);
            this.label29.TabIndex = 28;
            this.label29.Text = "Agregar empleados";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(144)))), ((int)(((byte)(169)))), ((int)(((byte)(89)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Roboto", 12F);
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(518, 424);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(103, 39);
            this.button1.TabIndex = 26;
            this.button1.Text = "Guardar";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(771, 367);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(89, 13);
            this.label12.TabIndex = 24;
            this.label12.Text = "ID departamento ";
            this.label12.Click += new System.EventHandler(this.label12_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(515, 367);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(80, 13);
            this.label13.TabIndex = 22;
            this.label13.Text = "ID tipo de pago";
            // 
            // txtsueldo
            // 
            this.txtsueldo.Location = new System.Drawing.Point(774, 340);
            this.txtsueldo.Name = "txtsueldo";
            this.txtsueldo.Size = new System.Drawing.Size(243, 20);
            this.txtsueldo.TabIndex = 19;
            this.txtsueldo.TextChanged += new System.EventHandler(this.label12_Click);
            this.txtsueldo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.idmostrar_KeyPress);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(771, 324);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(40, 13);
            this.label11.TabIndex = 18;
            this.label11.Text = "Sueldo";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(771, 280);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(89, 13);
            this.label8.TabIndex = 16;
            this.label8.Text = "Fecha de ingreso";
            // 
            // txtenfermedad
            // 
            this.txtenfermedad.Location = new System.Drawing.Point(518, 296);
            this.txtenfermedad.Name = "txtenfermedad";
            this.txtenfermedad.Size = new System.Drawing.Size(229, 20);
            this.txtenfermedad.TabIndex = 15;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(515, 280);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(64, 13);
            this.label9.TabIndex = 14;
            this.label9.Text = "Enfermedad";
            // 
            // txtnit
            // 
            this.txtnit.Location = new System.Drawing.Point(774, 253);
            this.txtnit.Name = "txtnit";
            this.txtnit.Size = new System.Drawing.Size(244, 20);
            this.txtnit.TabIndex = 13;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(771, 237);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(25, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "NIT";
            // 
            // txtdui
            // 
            this.txtdui.Location = new System.Drawing.Point(517, 253);
            this.txtdui.Name = "txtdui";
            this.txtdui.Size = new System.Drawing.Size(230, 20);
            this.txtdui.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(515, 237);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(26, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "DUI";
            // 
            // txttelefono
            // 
            this.txttelefono.Location = new System.Drawing.Point(518, 340);
            this.txttelefono.Name = "txttelefono";
            this.txttelefono.Size = new System.Drawing.Size(229, 20);
            this.txttelefono.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(513, 324);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Teléfono";
            // 
            // txtdireccion
            // 
            this.txtdireccion.Location = new System.Drawing.Point(518, 208);
            this.txtdireccion.Name = "txtdireccion";
            this.txtdireccion.Size = new System.Drawing.Size(499, 20);
            this.txtdireccion.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(513, 192);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Dirección";
            // 
            // txtcodigo
            // 
            this.txtcodigo.Location = new System.Drawing.Point(516, 123);
            this.txtcodigo.Name = "txtcodigo";
            this.txtcodigo.Size = new System.Drawing.Size(193, 20);
            this.txtcodigo.TabIndex = 5;
            this.txtcodigo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.idmostrar_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(513, 107);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Código";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // txtapellido
            // 
            this.txtapellido.Location = new System.Drawing.Point(774, 165);
            this.txtapellido.Name = "txtapellido";
            this.txtapellido.Size = new System.Drawing.Size(243, 20);
            this.txtapellido.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(771, 149);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Apellido";
            // 
            // txtnombre
            // 
            this.txtnombre.Location = new System.Drawing.Point(516, 165);
            this.txtnombre.Name = "txtnombre";
            this.txtnombre.Size = new System.Drawing.Size(229, 20);
            this.txtnombre.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(513, 149);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nombre";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Presentacion.Properties.Resources.Supermarket_workers_pana;
            this.pictureBox2.Location = new System.Drawing.Point(-4, -3);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(445, 478);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 27;
            this.pictureBox2.TabStop = false;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.smod);
            this.tabPage2.Controls.Add(this.tmod);
            this.tabPage2.Controls.Add(this.emod);
            this.tabPage2.Controls.Add(this.duimod);
            this.tabPage2.Controls.Add(this.nitmod);
            this.tabPage2.Controls.Add(this.dmod);
            this.tabPage2.Controls.Add(this.amod);
            this.tabPage2.Controls.Add(this.nmod);
            this.tabPage2.Controls.Add(this.cmod);
            this.tabPage2.Controls.Add(this.idmod);
            this.tabPage2.Controls.Add(this.cbd2);
            this.tabPage2.Controls.Add(this.cbtp2);
            this.tabPage2.Controls.Add(this.dateTimePicker2);
            this.tabPage2.Controls.Add(this.label30);
            this.tabPage2.Controls.Add(this.button4);
            this.tabPage2.Controls.Add(this.button3);
            this.tabPage2.Controls.Add(this.label25);
            this.tabPage2.Controls.Add(this.button2);
            this.tabPage2.Controls.Add(this.label10);
            this.tabPage2.Controls.Add(this.label14);
            this.tabPage2.Controls.Add(this.label15);
            this.tabPage2.Controls.Add(this.label16);
            this.tabPage2.Controls.Add(this.label17);
            this.tabPage2.Controls.Add(this.label18);
            this.tabPage2.Controls.Add(this.label19);
            this.tabPage2.Controls.Add(this.label20);
            this.tabPage2.Controls.Add(this.label21);
            this.tabPage2.Controls.Add(this.label22);
            this.tabPage2.Controls.Add(this.label23);
            this.tabPage2.Controls.Add(this.label24);
            this.tabPage2.Controls.Add(this.pictureBox3);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1060, 478);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Actualizar/Eliminar";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // smod
            // 
            this.smod.Location = new System.Drawing.Point(286, 366);
            this.smod.Name = "smod";
            this.smod.Size = new System.Drawing.Size(203, 20);
            this.smod.TabIndex = 70;
            // 
            // tmod
            // 
            this.tmod.Location = new System.Drawing.Point(43, 364);
            this.tmod.Name = "tmod";
            this.tmod.Size = new System.Drawing.Size(206, 20);
            this.tmod.TabIndex = 69;
            // 
            // emod
            // 
            this.emod.Location = new System.Drawing.Point(43, 322);
            this.emod.Name = "emod";
            this.emod.Size = new System.Drawing.Size(206, 20);
            this.emod.TabIndex = 68;
            // 
            // duimod
            // 
            this.duimod.Location = new System.Drawing.Point(43, 283);
            this.duimod.Name = "duimod";
            this.duimod.Size = new System.Drawing.Size(206, 20);
            this.duimod.TabIndex = 67;
            // 
            // nitmod
            // 
            this.nitmod.Location = new System.Drawing.Point(286, 283);
            this.nitmod.Name = "nitmod";
            this.nitmod.Size = new System.Drawing.Size(203, 20);
            this.nitmod.TabIndex = 66;
            // 
            // dmod
            // 
            this.dmod.Location = new System.Drawing.Point(43, 238);
            this.dmod.Name = "dmod";
            this.dmod.Size = new System.Drawing.Size(446, 20);
            this.dmod.TabIndex = 65;
            // 
            // amod
            // 
            this.amod.Location = new System.Drawing.Point(286, 199);
            this.amod.Name = "amod";
            this.amod.Size = new System.Drawing.Size(203, 20);
            this.amod.TabIndex = 64;
            // 
            // nmod
            // 
            this.nmod.Location = new System.Drawing.Point(43, 199);
            this.nmod.Name = "nmod";
            this.nmod.Size = new System.Drawing.Size(206, 20);
            this.nmod.TabIndex = 63;
            // 
            // cmod
            // 
            this.cmod.Location = new System.Drawing.Point(43, 156);
            this.cmod.Name = "cmod";
            this.cmod.Size = new System.Drawing.Size(206, 20);
            this.cmod.TabIndex = 62;
            // 
            // idmod
            // 
            this.idmod.Location = new System.Drawing.Point(43, 85);
            this.idmod.Name = "idmod";
            this.idmod.Size = new System.Drawing.Size(446, 20);
            this.idmod.TabIndex = 61;
            // 
            // cbd2
            // 
            this.cbd2.FormattingEnabled = true;
            this.cbd2.Location = new System.Drawing.Point(286, 405);
            this.cbd2.Name = "cbd2";
            this.cbd2.Size = new System.Drawing.Size(203, 21);
            this.cbd2.TabIndex = 60;
            // 
            // cbtp2
            // 
            this.cbtp2.FormattingEnabled = true;
            this.cbtp2.Location = new System.Drawing.Point(43, 404);
            this.cbtp2.Name = "cbtp2";
            this.cbtp2.Size = new System.Drawing.Size(206, 21);
            this.cbtp2.TabIndex = 59;
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(286, 324);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(203, 20);
            this.dateTimePicker2.TabIndex = 58;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Roboto Medium", 24F, System.Drawing.FontStyle.Bold);
            this.label30.Location = new System.Drawing.Point(38, 19);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(337, 38);
            this.label30.TabIndex = 57;
            this.label30.Text = "Modificar empleados";
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(46)))), ((int)(((byte)(101)))));
            this.button4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Roboto", 12F);
            this.button4.ForeColor = System.Drawing.Color.White;
            this.button4.Location = new System.Drawing.Point(388, 432);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(101, 30);
            this.button4.TabIndex = 55;
            this.button4.Text = "Eliminar";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.DarkBlue;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Roboto", 10F);
            this.button3.ForeColor = System.Drawing.Color.White;
            this.button3.Location = new System.Drawing.Point(388, 111);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(101, 28);
            this.button3.TabIndex = 54;
            this.button3.Text = "Buscar";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click_1);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(40, 69);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(67, 13);
            this.label25.TabIndex = 52;
            this.label25.Text = "ID empleado";
            this.label25.Click += new System.EventHandler(this.label25_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(144)))), ((int)(((byte)(169)))), ((int)(((byte)(89)))));
            this.button2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Roboto", 12F);
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(261, 432);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(109, 30);
            this.button2.TabIndex = 51;
            this.button2.Text = "Actualizar";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(286, 390);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(86, 13);
            this.label10.TabIndex = 49;
            this.label10.Text = "ID departamento";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(41, 387);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(80, 13);
            this.label14.TabIndex = 47;
            this.label14.Text = "ID tipo de pago";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(286, 350);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(40, 13);
            this.label15.TabIndex = 45;
            this.label15.Text = "Sueldo";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(286, 306);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(89, 13);
            this.label16.TabIndex = 43;
            this.label16.Text = "Fecha de ingreso";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(42, 306);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(64, 13);
            this.label17.TabIndex = 41;
            this.label17.Text = "Enfermedad";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(283, 267);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(25, 13);
            this.label18.TabIndex = 39;
            this.label18.Text = "NIT";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(42, 263);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(26, 13);
            this.label19.TabIndex = 37;
            this.label19.Text = "DUI";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(40, 348);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(49, 13);
            this.label20.TabIndex = 35;
            this.label20.Text = "Teléfono";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(42, 222);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(52, 13);
            this.label21.TabIndex = 33;
            this.label21.Text = "Dirección";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(41, 140);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(40, 13);
            this.label22.TabIndex = 31;
            this.label22.Text = "Código";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(283, 183);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(44, 13);
            this.label23.TabIndex = 29;
            this.label23.Text = "Apellido";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(40, 183);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(50, 13);
            this.label24.TabIndex = 27;
            this.label24.Text = "Nombre: ";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Presentacion.Properties.Resources.Statistics_bro;
            this.pictureBox3.Location = new System.Drawing.Point(596, 0);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(464, 475);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 56;
            this.pictureBox3.TabStop = false;
            // 
            // Esaldo
            // 
            this.Esaldo.Location = new System.Drawing.Point(0, 0);
            this.Esaldo.Name = "Esaldo";
            this.Esaldo.Size = new System.Drawing.Size(100, 20);
            this.Esaldo.TabIndex = 0;
            // 
            // Eenfermedad
            // 
            this.Eenfermedad.Location = new System.Drawing.Point(0, 0);
            this.Eenfermedad.Name = "Eenfermedad";
            this.Eenfermedad.Size = new System.Drawing.Size(100, 20);
            this.Eenfermedad.TabIndex = 0;
            // 
            // Enit
            // 
            this.Enit.Location = new System.Drawing.Point(0, 0);
            this.Enit.Name = "Enit";
            this.Enit.Size = new System.Drawing.Size(100, 20);
            this.Enit.TabIndex = 0;
            // 
            // Edui
            // 
            this.Edui.Location = new System.Drawing.Point(0, 0);
            this.Edui.Name = "Edui";
            this.Edui.Size = new System.Drawing.Size(100, 20);
            this.Edui.TabIndex = 0;
            // 
            // Etelefono
            // 
            this.Etelefono.Location = new System.Drawing.Point(0, 0);
            this.Etelefono.Name = "Etelefono";
            this.Etelefono.Size = new System.Drawing.Size(100, 20);
            this.Etelefono.TabIndex = 0;
            // 
            // Edireccion
            // 
            this.Edireccion.Location = new System.Drawing.Point(0, 0);
            this.Edireccion.Name = "Edireccion";
            this.Edireccion.Size = new System.Drawing.Size(100, 20);
            this.Edireccion.TabIndex = 0;
            // 
            // Ecodigo
            // 
            this.Ecodigo.Location = new System.Drawing.Point(0, 0);
            this.Ecodigo.Name = "Ecodigo";
            this.Ecodigo.Size = new System.Drawing.Size(100, 20);
            this.Ecodigo.TabIndex = 0;
            // 
            // Eapellido
            // 
            this.Eapellido.Location = new System.Drawing.Point(0, 0);
            this.Eapellido.Name = "Eapellido";
            this.Eapellido.Size = new System.Drawing.Size(100, 20);
            this.Eapellido.TabIndex = 0;
            // 
            // Enombre
            // 
            this.Enombre.Location = new System.Drawing.Point(0, 0);
            this.Enombre.Name = "Enombre";
            this.Enombre.Size = new System.Drawing.Size(100, 20);
            this.Enombre.TabIndex = 0;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Roboto Medium", 24F, System.Drawing.FontStyle.Bold);
            this.label27.Location = new System.Drawing.Point(12, 23);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(354, 38);
            this.label27.TabIndex = 2;
            this.label27.Text = "Módulo de Empleados";
            // 
            // button9
            // 
            this.button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button9.ForeColor = System.Drawing.Color.Transparent;
            this.button9.Image = global::Presentacion.Properties.Resources.menu__1_;
            this.button9.Location = new System.Drawing.Point(1035, 23);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(41, 38);
            this.button9.TabIndex = 5;
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button8
            // 
            this.button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button8.ForeColor = System.Drawing.Color.Transparent;
            this.button8.Image = global::Presentacion.Properties.Resources.right;
            this.button8.Location = new System.Drawing.Point(986, 23);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(41, 38);
            this.button8.TabIndex = 4;
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button7
            // 
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.ForeColor = System.Drawing.Color.Transparent;
            this.button7.Image = global::Presentacion.Properties.Resources.left;
            this.button7.Location = new System.Drawing.Point(937, 23);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(41, 38);
            this.button7.TabIndex = 3;
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // Empleados
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1093, 602);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Empleados";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Empleado";
            this.Load += new System.EventHandler(this.Empleados_Load);
            this.tabControl1.ResumeLayout(false);
            this.Mostrar.ResumeLayout(false);
            this.Mostrar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage Mostrar;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtcodigo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtapellido;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtnombre;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtsueldo;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtenfermedad;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtnit;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtdui;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txttelefono;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtdireccion;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button3;
        //private System.Windows.Forms.TextBox Eid;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox Esaldo;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox Eenfermedad;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox Enit;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox Edui;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox Etelefono;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox Edireccion;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox Ecodigo;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox Eapellido;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox Enombre;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.TextBox idmostrar;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.ComboBox cbd;
        private System.Windows.Forms.ComboBox cbtp;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.ComboBox cbd2;
        private System.Windows.Forms.ComboBox cbtp2;
        private System.Windows.Forms.TextBox smod;
        private System.Windows.Forms.TextBox tmod;
        private System.Windows.Forms.TextBox emod;
        private System.Windows.Forms.TextBox duimod;
        private System.Windows.Forms.TextBox nitmod;
        private System.Windows.Forms.TextBox dmod;
        private System.Windows.Forms.TextBox amod;
        private System.Windows.Forms.TextBox nmod;
        private System.Windows.Forms.TextBox cmod;
        private System.Windows.Forms.TextBox idmod;
    }
}