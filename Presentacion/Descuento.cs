﻿using AccesoDatos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class Descuento : Form
    {
        public Descuento()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Descuentos ObjDesc = new Descuentos();
            try
            {
                ObjDesc.Tipo = int.Parse(textBox1.Text);
                ObjDesc.Cantidad = int.Parse(textBox2.Text);
                ObjDesc.Descuento = textBox3.Text;
                ObjDesc.Empleado = int.Parse(textBox4.Text);

                bool respuestaSQL = ObjDesc.InsertarDescuento();
                if (respuestaSQL == true)
                {
                    MessageBox.Show("Los datos del nuevo cliente fueron insertados correctamente");
                    textBox1.Text = "";
                    textBox2.Text = "";
                    textBox3.Text = "";
                    textBox4.Text = "";
                }
                else
                {
                    MessageBox.Show(ObjDesc.Mensaje);
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show("Error!: " + Ex.Message + " " + ObjDesc.Mensaje);
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Descuentos ObjDesc = new Descuentos();
            try
            {
                DataSet DatosDesc = ObjDesc.ConsultarDescuento(int.Parse(textBox6.Text));
                int numregistros = DatosDesc.Tables["TablaConsultada"].Rows.Count;
                if (numregistros == 0)
                {
                    MessageBox.Show("No existe en la tabla Descuentos este identificador");
                }
                else
                {
                    textBox7.Text = DatosDesc.Tables["TablaConsultada"].Rows[0]["idtipodesc"].ToString();
                    textBox8.Text = DatosDesc.Tables["TablaConsultada"].Rows[0]["cantidad"].ToString();
                    textBox9.Text = DatosDesc.Tables["TablaConsultada"].Rows[0]["descuento"].ToString();
                    textBox10.Text = DatosDesc.Tables["TablaConsultada"].Rows[0]["idempleado"].ToString();
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show("Fatality!: " + Ex.Message + " " + ObjDesc.Mensaje);
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            Descuentos ObjDesc = new Descuentos();
            try
            {
                ObjDesc.Identificador = int.Parse(textBox6.Text);
                ObjDesc.Tipo = int.Parse(textBox7.Text);
                ObjDesc.Cantidad = int.Parse(textBox8.Text);
                ObjDesc.Descuento = textBox9.Text;
                ObjDesc.Empleado = int.Parse(textBox10.Text);

                bool respuestaSQL = ObjDesc.ActualizarDescuento();
                if (respuestaSQL == true)
                {
                    MessageBox.Show("Los datos fueron actualizados correctamente");
                    textBox6.Text = "";
                    textBox7.Text = "";
                    textBox8.Text = "";
                    textBox9.Text = "";
                    textBox10.Text = "";
                }
                else
                {
                    MessageBox.Show(ObjDesc.Mensaje);
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show("Fatality!: " + Ex.Message + " " + ObjDesc.Mensaje);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Descuentos ObjDesc = new Descuentos();
            try
            {
                bool respuestaSQL = ObjDesc.EliminarDescuento(textBox6.Text);
                if (respuestaSQL == true)
                {
                    MessageBox.Show("Los datos de cliente fueron Eliminados correctamente");
                    textBox6.Text = "";
                    textBox7.Text = "";
                    textBox8.Text = "";
                    textBox9.Text = "";
                    textBox10.Text = "";
                }
                else
                {
                    MessageBox.Show(ObjDesc.Mensaje);
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show("Fatality!: " + Ex.Message + " " + ObjDesc.Mensaje);
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void Descuento_Load(object sender, EventArgs e)
        {
            Descuentos ObjDesc = new Descuentos();
            try
            {
                DataSet DatosDesc = ObjDesc.ConsultarTodosDescuento();
                dataGridView1.DataSource = DatosDesc.Tables["TablaConsultada"].DefaultView;
            }
            catch (Exception Ex)
            {
                MessageBox.Show("Fatality: " + Ex.Message + " " + ObjDesc.Mensaje);
            }
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {

        }

        private void button5_Click_1(object sender, EventArgs e)
        {
            Descuentos ObjDesc = new Descuentos();
            try
            {
                if (string.IsNullOrEmpty(textBox11.Text ?? string.Empty))
                {
                    MessageBox.Show("Ingrese el id del cliente por favor");
                    return;
                }
                int iddesc = int.Parse(textBox11.Text);
                DataSet DatosDesc = ObjDesc.ConsultarDescuento(iddesc);
                dataGridView1.DataSource = DatosDesc.Tables["TablaConsultada"].DefaultView;
            }
            catch (Exception Ex)
            {
                MessageBox.Show("Fatality: " + Ex.Message + " " + ObjDesc.Mensaje);
            }
        }

        private void button6_Click_1(object sender, EventArgs e)
        {
            Descuentos ObjDesc = new Descuentos();
            try
            {
                DataSet DatosDesc = ObjDesc.ConsultarTodosDescuento();
                dataGridView1.DataSource = DatosDesc.Tables["TablaConsultada"].DefaultView;
            }
            catch (Exception Ex)
            {
                MessageBox.Show("Fatality: " + Ex.Message + " " + ObjDesc.Mensaje);
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Empleados empleados = new Empleados();
            empleados.Show();
            this.Hide();

        }

        private void button9_Click(object sender, EventArgs e)
        {
            Menu menu = new Menu();
            menu.Show();
            this.Hide();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Menu menu = new Menu();
            menu.Show();
            this.Hide();
        }
    }
}
