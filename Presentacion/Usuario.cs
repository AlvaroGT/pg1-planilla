﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AccesoDatos;

namespace Presentacion
{
    public partial class Usuario : Form
    {
        public Usuario()
        {
            InitializeComponent();
            Select select = new Select();
            tipo.DataSource = select.Listarol().Tables["TablaConsultada"];
            tipo.DisplayMember = "rol";
            tipo.ValueMember = "idrol";

            tipomod.DataSource = select.Listarol().Tables["TablaConsultada"];
            tipomod.DisplayMember = "rol";
            tipomod.ValueMember = "idrol";

        }

        private void label14_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            UsuarioDAO usuario = new UsuarioDAO();
            try
            {
                DataSet lista = usuario.ListaMostrar();
                dataGridView1.DataSource = lista.Tables["TablaConsultada"].DefaultView;
            }
            catch (Exception Ex)
            {
                MessageBox.Show("ERROR: " + Ex.Message);
            }
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Usuario_Load(object sender, EventArgs e)
        {
            UsuarioDAO usuario = new UsuarioDAO();
            try
            {
                DataSet lista = usuario.ListaMostrar();
                dataGridView1.DataSource = lista.Tables["TablaConsultada"].DefaultView;
            }
            catch (Exception Ex)
            {
                MessageBox.Show("ERROR: " + Ex.Message);
            }

        }

        private void button5_Click(object sender, EventArgs e)
        {
            UsuarioDAO usuario = new UsuarioDAO();
            if (string.IsNullOrEmpty(idconsulta.Text ?? string.Empty))
            {
                MessageBox.Show("Ingrese el id del Usuario por favor");
                return;
            }
            try
            {
                DataSet usu = usuario.Consultar(int.Parse(idconsulta.Text));
                dataGridView1.DataSource = usu.Tables["TablaConsultada"].DefaultView;
            }
            catch (Exception Ex)
            {
                MessageBox.Show("ERROR: " + Ex.Message);
            }
        }

        private void textBox1_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            UsuarioDAO usuario = new UsuarioDAO();
            try
            {
                usuario.usuario = nombre.Text;
                usuario.contrasena = contra.Text;
                usuario.idrol = int.Parse(tipo.SelectedValue.ToString());
                if (usuario.insertar())
                {
                    MessageBox.Show("NUEVO USUARIO AGREGADO");
                    nombre.Text = "";
                    contra.Text = "";
                }
                else
                {
                    MessageBox.Show("No se pudo insertar el usuario");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR: " + ex.Message);
            }

        }

        private void button6_Click(object sender, EventArgs e)
        {
            UsuarioDAO usuario = new UsuarioDAO();
            try
            {
                DataSet lista = usuario.ListaMostrar();
                dataGridView1.DataSource = lista.Tables["TablaConsultada"].DefaultView;
            }
            catch (Exception Ex)
            {
                MessageBox.Show("ERROR: " + Ex.Message);
            }
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            UsuarioDAO usuarios = new UsuarioDAO();
            try
            {
                DataSet usuario = usuarios.Consultar(int.Parse(idmod.Text));
                if (usuario.Tables["Tablaconsultada"].Rows.Count == 0)
                {
                    MessageBox.Show("No hay Usuarios con ese id");
                }
                else {
                    idmod.Text = usuario.Tables["Tablaconsultada"].Rows[0]["idusuario"].ToString();
                    usuariomod.Text = usuario.Tables["Tablaconsultada"].Rows[0]["usuario"].ToString();
                    contramod.Text = usuario.Tables["Tablaconsultada"].Rows[0]["contrasena"].ToString();
                    tipomod.SelectedValue = usuario.Tables["Tablaconsultada"].Rows[0]["idrol"].ToString();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            UsuarioDAO usuario = new UsuarioDAO();
            try
            {
                usuario.idusuario = int.Parse(idmod.Text);
                usuario.usuario = usuariomod.Text;
                usuario.contrasena = contramod.Text;
                usuario.idrol = int.Parse(tipomod.SelectedValue.ToString());
                if (usuario.Actualizar())
                {
                    MessageBox.Show("USUARIO ACTUALIZADO CON EXITO");
                    usuariomod.Text = "";
                    contramod.Text = "";
                    idmod.Text = "";
                }
                else
                {
                    MessageBox.Show("No se pudo actualizar el usuario");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR: " + ex.Message);
            }
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            UsuarioDAO usuario = new UsuarioDAO();
            try
            {
                if (usuario.Eliminar(int.Parse(idmod.Text)))
                {
                    MessageBox.Show("USUARIO ELIMINADO CON EXITO");
                    usuariomod.Text = "";
                    contramod.Text = "";
                    idmod.Text = "";
                }
                else
                {
                    MessageBox.Show("No se pudo Eliminar el usuario");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR: " + ex.Message);
            }
        }

        private void idmod_TextChanged(object sender, EventArgs e)
        {

        }

        private void idmod_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 32 && e.KeyChar <= 43) || (e.KeyChar >= 58 && e.KeyChar <= 255) || e.KeyChar == 45 || e.KeyChar == 47)
            {
                MessageBox.Show("Solo números, punto, y coma", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void idconsulta_TextChanged(object sender, EventArgs e)
        {

        }

        private void button8_Click(object sender, EventArgs e)
        {

        }

        private void button7_Click(object sender, EventArgs e)
        {
            DetaPlanilla det = new DetaPlanilla();
            det.Show();
            this.Hide();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            Menu menu = new Menu();
            menu.Show();
            this.Hide();
        }
    }
}
