﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class Menu : Form
    {
        Empleados empleados = new Empleados();

        public Menu()
        {
            InitializeComponent();
            
        }

        private void btnEmpleados_Click(object sender, EventArgs e)
        {
            empleados.Show();
            this.Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Descuento descuento = new Descuento();
            descuento.Show();
            this.Hide();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Usuario usuario = new Usuario();
            usuario.Show();
            this.Hide();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Usuario usuario = new Usuario();
            usuario.Show();
            this.Hide();
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            Planilla planilla = new Planilla();
            planilla.Show();
            this.Hide();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            DetaPlanilla d = new DetaPlanilla();
            d.Show();
            this.Hide();
        }
    }
}
