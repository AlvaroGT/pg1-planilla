﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;

namespace AccesoDatos
{
    public class DetPlanillaDAO : ConexionSQL
    {
        public long Identificador;
        private int idplanilla;
        private int idempleados;
        private int iddescuentos;
        private int horasextradiu;
        private int horasextranoc;
        private string bonos;
        private int horasdiu;
        private int horasnoc;
        private string isss;
        private string afp;
        private string estadorenta;
        private string renta;
        private string salarioneto;

        public long Identificacion
        {
            get { return Identificador; }
            set { Identificador = value; }
        }
        public int Idplanilla
        {
            get { return idplanilla; }
            set { idplanilla = value; }
        }
        public int Idempleados
        {
            get { return idempleados; }
            set { idempleados = value; }
        }
        public int Iddescuentos
        {
            get { return iddescuentos; }
            set { iddescuentos = value; }
        }
        public int Horasextradiu
        {
            get { return horasextradiu; }
            set { horasextradiu = value; }
        }
        public int Horasextranoc
        {
            get { return horasextranoc; }
            set { horasextranoc = value; }
        }
        public string Bonos
        {
            get { return bonos; }
            set { bonos = value; }
        }
        public int Horasdiu
        {
            get { return horasdiu; }
            set { horasdiu = value; }
        }
        public int Horasnoc
        {
            get { return horasnoc; }
            set { horasnoc = value; }
        }
        public string Isss
        {
            get { return isss; }
            set { isss = value; }
        }
        public string Afp
        {
            get { return afp; }
            set { afp = value; }
        }
        public string Estadorenta
        {
            get { return estadorenta; }
            set { estadorenta = value; }
        }
        public string Renta
        {
            get { return renta; }
            set { renta = value; }
        }
        public string Salarioneto
        {
            get { return salarioneto; }
            set { salarioneto = value; }
        }

        public bool InsertarDatos()
        {
            string cadenaSQLInsertar = "INSERT INTO detalleplanilla (idplanilla, idempleados, iddescuentos, horasextradiu, horasextranoc, bonos, horasdiu, horasnoc, isss, afp, estadorenta, renta, salarioneto) VALUES('"
                + this.idplanilla + "','" + this.idempleados + "','" + this.iddescuentos + "','" 
                + this.horasextradiu + "','" + this.horasextranoc + "','" + this.bonos + "','" 
                + this.horasdiu + "','" + this.horasnoc + "','" + this.isss + "','" 
                + this.afp + "','" + this.estadorenta + "','" + this.renta + "','" + this.salarioneto + "')";
            bool respuestaSQL = EjecutarSQL(cadenaSQLInsertar);
            return respuestaSQL;
        }
        public DataSet ConsultarDatos(int id)
        {
            string cadenaSQLConsultar = "SELECT * FROM detalleplanilla WHERE [iddetalleplanilla] = " + id + "";
            DataSet ConsultaResultante = ConsultarSQL(cadenaSQLConsultar);
            return ConsultaResultante;
        }
        public DataSet ConsultarTodosDatos()
        {
            string cadenaSQLConsultar = "SELECT * FROM detalleplanilla";
            DataSet ConsultaResultante = ConsultarSQL(cadenaSQLConsultar);
            return ConsultaResultante;
        }
        public bool ActualizarDatos()
        {
            string cadenaSQLActualizar = "UPDATE detalleplanilla SET idplanilla = '" + this.idplanilla 
                + "', idempleados = '" + this.idempleados + "', iddescuentos = '" + this.iddescuentos
                + "', horasextradiu = '" + this.horasextradiu + "', horasextranoc = '" + this.horasextranoc
                + "', bonos = '" + this.bonos + "', horasdiu = '" + this.horasdiu
                + "', horasnoc = '" + this.horasnoc + "', isss = '" + this.isss
                + "', afp = '" + this.afp + "', estadorenta = '" + this.estadorenta
                + "', renta = '" + this.renta + "', salarioneto = '" + this.salarioneto
                + "' WHERE (iddetalleplanilla= " + this.Identificador + ")";
            bool respuestaSQL = EjecutarSQL(cadenaSQLActualizar);
            return respuestaSQL;
        }
        public bool EliminarDatos(string id)
        {
            string cadenaSQLEliminar = "DELETE FROM detalleplanilla WHERE iddetalleplanilla = " + id + "";
            bool respuestaSQL = EjecutarSQL(cadenaSQLEliminar);
            return respuestaSQL;
        }
    }
}
