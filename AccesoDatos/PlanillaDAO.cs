﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;

namespace AccesoDatos
{
    public class PlanillaDAO : ConexionSQL
    {
        public long Identificador;
        private string mes;
        private string parte;

        public long Identificacion
        {
            get { return Identificador; }
            set { Identificador = value; }
        }
        public string Mes
        {
            get { return mes; }
            set { mes = value; }
        }
        public string Parte
        {
            get { return parte; }
            set { parte = value; }
        }

        public bool InsertarDatos()
        {
            string cadenaSQLInsertar = "INSERT INTO planilla (mes, parte) VALUES('" + this.mes + "','" + this.parte + "')";
            bool respuestaSQL = EjecutarSQL(cadenaSQLInsertar);
            return respuestaSQL;
        }
        public DataSet ConsultarDatos(int id)
        {
            string cadenaSQLConsultar = "SELECT * FROM planilla WHERE [idplanilla] = " + id + "";
            DataSet ConsultaResultante = ConsultarSQL(cadenaSQLConsultar);
            return ConsultaResultante;
        }
        public DataSet ConsultarTodosDatos()
        {
            string cadenaSQLConsultar = "SELECT * FROM planilla";
            DataSet ConsultaResultante = ConsultarSQL(cadenaSQLConsultar);
            return ConsultaResultante;
        }
        public bool ActualizarDatos()
        {
            string cadenaSQLActualizar = "UPDATE planilla SET mes = '" + this.mes + "', parte = '" + this.parte + "' WHERE (idplanilla= " + this.Identificador + ")";
            bool respuestaSQL = EjecutarSQL(cadenaSQLActualizar);
            return respuestaSQL;
        }
        public bool EliminarDatos(string id)
        {
            string cadenaSQLEliminar = "DELETE FROM planilla WHERE idplanilla = " + id + "";
            bool respuestaSQL = EjecutarSQL(cadenaSQLEliminar);
            return respuestaSQL;
        }
    }
}
