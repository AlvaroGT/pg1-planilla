﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;


namespace AccesoDatos
{
    public class UsuarioDAO:ConexionSQL
    {
        public int idusuario { get; set; }
        public string usuario { get; set; }
        public string contrasena { get; set; }
        public int idrol { get; set; }

        public bool Login(string usuario, string contra) {
            using (var conex = obtenerConex()) {
                conex.Open();
                using (var command = new SqlCommand()) {
                    command.Connection = conex;
                    command.CommandText = "Select * from usuario where usuario=@nombre and contrasena=@contra";
                    command.Parameters.AddWithValue("@nombre", usuario);
                    command.Parameters.AddWithValue("@contra", contra);
                    command.CommandType = CommandType.Text;
                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.HasRows)
                    {
                        Console.WriteLine("======================================");
                        //Console.WriteLine(reader.GetString(1) + "," + reader.GetString(2) + "," + reader.GetString(3));
                        while (reader.Read()) {
                            Console.WriteLine((string)(reader[2]));
                        }                        Console.WriteLine("======================================");
                        return true;
                    }
                    else {
                        return false;
                    }
                }
            }
        }



        public bool insertar() {
            bool flag = false;
            string SQLinsertar = "insert into usuario (usuario, contrasena, idrol) values ('"+this.usuario + "','" + this.contrasena + "', " + this.idrol + ");";
            flag = EjecutarSQL(SQLinsertar);
            return flag;
        }
        public bool Actualizar()
        {
            bool flag = false;
            string SQLActualizar = "UPDATE usuario SET usuario = '" + this.usuario + "', contrasena = '" + this.contrasena + "', idrol = " + this.idrol + " where idusuario = " + this.idusuario + ";";
            flag = EjecutarSQL(SQLActualizar);
            return flag;
        }
        public DataSet Consultar(int id)
        {
            string SQLConsultar = "SELECT * FROM usuario where idusuario = " + id.ToString() + ";";
            DataSet consultaResultante = ConsultarSQL(SQLConsultar);
            return consultaResultante;
        }
        public bool Eliminar(int id)
        {
            string SQLEliminar = "DELETE FROM usuario WHERE idusuario =" + id.ToString() + ";";
            bool respuestaSQL = EjecutarSQL(SQLEliminar);
            return respuestaSQL;
        }
        public DataSet Lista()
        {
            string SQLListar = "SELECT * FROM usuario";
            DataSet ConsultaResultante = ConsultarSQL(SQLListar);
            return ConsultaResultante;
        }

        public DataSet ListaMostrar()
        {
            string SQLListar = "select u.idusuario, u.usuario, u.contrasena, r.rol from usuario u inner join roles r on u.idrol = r.idrol";
            DataSet ConsultaResultante = ConsultarSQL(SQLListar);
            return ConsultaResultante;
        }
    }
}
