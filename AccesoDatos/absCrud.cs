﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccesoDatos
{
    public class absCrud : ConexionSQL
    {
        /*
        public absCrud(string insertar, string actualizar, string consultar, string eliminar, string listar) {
            SQLinsertar = insertar;
            SQLActualizar = actualizar;
            SQLConsultar = consultar;
            SQLEliminar = eliminar;
            SQLListar = listar;
        }
        */

        public virtual string SQLinsertar() { return ""; }
        public string SQLActualizar { get; set; }
        public string SQLConsultar { get; set; }
        public string SQLEliminar { get; set; }
        public string SQLListar { get; set; }
        
        public virtual bool insertar()
        {
            bool respuestaSQL = EjecutarSQL(SQLinsertar());
            return respuestaSQL;
        }

        public virtual bool Actualizar()
        {
            bool respuestaSQL = EjecutarSQL(SQLActualizar);
            return respuestaSQL;
        }

        public virtual DataSet Consultar()
        {
            DataSet consultaResultante = ConsultarSQL(SQLConsultar);
            return consultaResultante;
        }

        public virtual bool Eliminar()
        {
            bool respuestaSQL = EjecutarSQL(SQLEliminar);
            return respuestaSQL;
        }

        public virtual DataSet Lista()
        {
            DataSet ConsultaResultante = ConsultarSQL(SQLListar);
            return ConsultaResultante;
        }
    }
}
