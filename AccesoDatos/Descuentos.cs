﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;

namespace AccesoDatos
{
    public class Descuentos : ConexionSQL
    {
        public long Identificador;
        private int tipo;
        private int cantidad;
        private string descuento;
        private int empleado;

        public long Identificacion
        {
            get { return Identificador; }
            set { Identificador = value; }
        }
        public int Tipo
        {
            get { return tipo; }
            set { tipo = value; }
        }
        public int Cantidad
        {
            get { return cantidad; }
            set { cantidad = value; }
        }
        public string Descuento
        {
            get { return descuento; }
            set { descuento = value; }
        }
        public int Empleado
        {
            get { return empleado; }
            set { empleado = value; }
        }
        public bool InsertarDescuento()
        {
            string cadenaSQLInsertar = "INSERT INTO descuentos (idtipodesc, cantidad, descuento, idempleado) VALUES('" + this.tipo + "','" + this.cantidad + "','" + this.descuento + "','" + this.empleado + "')";
            //string cadenaSQLInsertar = "INSERT INTO descuentos (iddescuentos,idtipodesc, cantidad, descuento, idempleado) VALUES('" + 8 + "','" + this.tipo + "','" + this.cantidad + "','" + this.descuento + "','" + this.empleado + "')";
            bool respuestaSQL = EjecutarSQL(cadenaSQLInsertar);
            return respuestaSQL;
        }
        public DataSet ConsultarDescuento(int id)
        {
            string cadenaSQLConsultar = "SELECT * FROM descuentos WHERE [iddescuentos] = " + id + "";
            DataSet ConsultaResultante = ConsultarSQL(cadenaSQLConsultar);
            return ConsultaResultante;
        }
        public DataSet ConsultarTodosDescuento()
        {
            string cadenaSQLConsultar = "SELECT * FROM descuentos";
            DataSet ConsultaResultante = ConsultarSQL(cadenaSQLConsultar);
            return ConsultaResultante;
        }
        public bool ActualizarDescuento()
        {
            string cadenaSQLActualizar = "UPDATE descuentos SET idtipodesc = '" + this.tipo + "', cantidad = '" + this.cantidad + "',descuento='" + this.descuento + "',idempleado='" + this.empleado + "' WHERE (iddescuentos= " + this.Identificador + ")";
            bool respuestaSQL = EjecutarSQL(cadenaSQLActualizar);
            return respuestaSQL;
        }
        public bool EliminarDescuento(string id)
        {
            string cadenaSQLEliminar = "DELETE FROM descuentos WHERE iddescuentos = " + id + "";
            bool respuestaSQL = EjecutarSQL(cadenaSQLEliminar);
            return respuestaSQL;
        }
    }
}
