﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccesoDatos
{
    interface crud
    {
        bool insertar();
        DataSet Consultar();
        DataSet Lista();
        bool Actualizar();
        bool Eliminar();
    }
}
