﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccesoDatos
{
    public class EmpleadoDAO:ConexionSQL
    {
        

        public int idempleado { get; set; }
        public int codempleado { get; set; }
        public string nombre { get; set; }
        public string apellido { get; set; }
        public string direccion { get; set; }
        public string telefono { get; set; }
        public string dui { get; set; }
        public string nit { get; set; }
        public string enfermedad { get; set; }
        public string fechaingreso { get; set; }
        public double sueldo { get; set; }
        public int idtipopago { get; set; }
        public int iddepartamento { get; set; }
        
        public bool insertar()
        {
            string SQLinsertar = "INSERT INTO empleado"+
            " (codempleado, nombre,"+
            " apellido, direccion, telefono,"+
            " dui, nit, enfermedad, fechaingreso, "+
            "sueldo, idtipopago, iddepartamento)VALUES"+
            "(" + this.codempleado + ",'"+ this.nombre +
            "','"+ this.apellido +"','"+ this.direccion +"','"+ this.telefono + "',"+
            "'"+ this.dui + "','" + this.nit + "','" + this.enfermedad + "'," + this.fechaingreso + 
            "," + this.sueldo +","+ this.idtipopago +","+ this.iddepartamento + ");";
            bool respuestaSQL = EjecutarSQL(SQLinsertar);

            return respuestaSQL;
        }

        public bool Actualizar()
        {
            string SQLActualizar = "UPDATE empleado SET codempleado =" + this.codempleado + ", nombre = '" + this.nombre + "' , apellido = '"+ this.apellido + "', direccion = '"+ this.direccion + "', telefono = '"+ this.telefono + "', dui = '"+ this.dui + "', nit = '"+ this.nit + "', enfermedad = '"+ this.enfermedad + "' , fechaingreso = '"+ this.fechaingreso + "', sueldo ="+ this.sueldo+", idtipopago = "+ this.idtipopago +", iddepartamento = "+ this.iddepartamento+" WHERE idempleado = "+ this.idempleado + ";";
            bool respuestaSQL = EjecutarSQL(SQLActualizar);
            return respuestaSQL;
        }

        public DataSet Consultar(int id)
        {
            string SQLConsultar = "SELECT * FROM empleado where idempleado = "+id.ToString()+";";
            DataSet consultaResultante = ConsultarSQL(SQLConsultar);
            return consultaResultante;
        }

        public bool Eliminar(int id)
        {
            string SQLEliminar = "DELETE FROM empleado WHERE idempleado =" + id.ToString()+";";
            bool respuestaSQL = EjecutarSQL(SQLEliminar);
            return respuestaSQL;
        }

        public DataSet Lista()
        {
            string SQLListar = "SELECT * FROM empleado";
            DataSet ConsultaResultante = ConsultarSQL(SQLListar);
            return ConsultaResultante;
        }

        public DataSet ListaMostrar()
        {
            string SQLListar = "SELECT A.idempleado, A.codempleado, A.nombre, A.apellido, A.direccion, A.telefono, A.dui, A.nit, A.enfermedad, A.fechaingreso, A.sueldo, B.tipopago, C.departamento FROM Planilla.dbo.empleado A INNER JOIN  Planilla.dbo.tipopagos B on A.idtipopago = B.idtipopago INNER JOIN Planilla.dbo.departamento C on A.iddepartamento = C.iddepartamento";
            DataSet ConsultaResultante = ConsultarSQL(SQLListar);
            return ConsultaResultante;
        }

    }
}
