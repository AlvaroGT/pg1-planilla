﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace AccesoDatos
{
    public abstract class ConexionSQL {

        SqlConnection conn;
        private string mensaje;
        public string Mensaje
        {
            get { return mensaje; }
            set { mensaje = value; }
        }

        public readonly string conexString;
        public ConexionSQL()
        {
            conexString = "workstation id=Planilla.mssql.somee.com;packet size=4096;user id=topaso8107_SQLLogin_1;pwd=2cze7ssmlu;data source=Planilla.mssql.somee.com;persist security info=False;initial catalog=Planilla";

            //Cadena de Conexion Para SQL Server:
            String cadenaconexion = "workstation id = Planilla.mssql.somee.com; packet size = 4096; user id = topaso8107_SQLLogin_1; pwd = 2cze7ssmlu; data source = Planilla.mssql.somee.com; persist security info = False; initial catalog = Planilla";
            conn = new SqlConnection(cadenaconexion);
        }

        protected SqlConnection obtenerConex() {
            return new SqlConnection(conexString);
        }

        public DataSet ConsultarSQL(String SentenciaSQL)
        {
            try
            {
                conn.Open();
                SqlDataAdapter objRes = new SqlDataAdapter(SentenciaSQL, conn);
                DataSet datos = new DataSet();
                objRes.Fill(datos, "TablaConsultada");
                mensaje = "La consulta de datos fue Exitosa";
                return datos;
            }
            catch (Exception MiExc)
            {
                DataSet datos2 = new DataSet();
                mensaje = "ERROR FATALLITY: " + MiExc.Message;
                return datos2;
            }
            finally
            {
                conn.Close();
            }
        }
        public bool EjecutarSQL(String SentenciaSQL)
        {
            try
            {
                conn.Open();
                SqlCommand miComando = new SqlCommand();
                miComando.Connection = conn;
                miComando.CommandText = SentenciaSQL;
                miComando.ExecuteNonQuery();
                mensaje = "Proceso Ejecutado con Exito";
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                mensaje = "Tenemos el siguiente Fatality: " + e.Message;
                return false;
            }
            finally
            {
                conn.Close();
            }
        }
    }
}
