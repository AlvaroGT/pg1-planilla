﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccesoDatos
{
    public class Select: ConexionSQL
    {

        public DataSet ListaTipodesc()
        {
            string SQLListar = "SELECT * FROM tipodesc";
            DataSet ConsultaResultante = ConsultarSQL(SQLListar);
            return ConsultaResultante;
        }

        public DataSet ListaTipopagos()
        {
            string SQLListar = "SELECT * FROM tipopagos";
            DataSet ConsultaResultante = ConsultarSQL(SQLListar);
            return ConsultaResultante;
        }

        public DataSet ListaDescuentos()
        {
            string SQLListar = "SELECT * FROM descuentos";
            DataSet ConsultaResultante = ConsultarSQL(SQLListar);
            return ConsultaResultante;
        }

        public DataSet ListaDepartamento()
        {
            string SQLListar = "SELECT * FROM departamento";
            DataSet ConsultaResultante = ConsultarSQL(SQLListar);
            return ConsultaResultante;
        }

        public DataSet Listarol()
        {
            string SQLListar = "SELECT * FROM roles";
            DataSet ConsultaResultante = ConsultarSQL(SQLListar);
            return ConsultaResultante;
        }

    }
}
