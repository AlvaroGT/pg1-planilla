USE [master]
GO
/****** Object:  Database [Planilla]    Script Date: 25/06/2021 10:10:08:Am ******/
CREATE DATABASE [Planilla]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Planilla_Data', FILENAME = N'c:\dzsqls\Planilla.mdf' , SIZE = 3264KB , MAXSIZE = 30720KB , FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Planilla_Logs', FILENAME = N'c:\dzsqls\Planilla.ldf' , SIZE = 1024KB , MAXSIZE = 30720KB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Planilla] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Planilla].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Planilla] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Planilla] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Planilla] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Planilla] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Planilla] SET ARITHABORT OFF 
GO
ALTER DATABASE [Planilla] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Planilla] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Planilla] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Planilla] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Planilla] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Planilla] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Planilla] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Planilla] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Planilla] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Planilla] SET  ENABLE_BROKER 
GO
ALTER DATABASE [Planilla] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Planilla] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Planilla] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Planilla] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Planilla] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Planilla] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Planilla] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Planilla] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Planilla] SET  MULTI_USER 
GO
ALTER DATABASE [Planilla] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Planilla] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Planilla] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Planilla] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [Planilla] SET DELAYED_DURABILITY = DISABLED 
GO
USE [Planilla]
GO
/****** Object:  User [topaso8107_SQLLogin_1]    Script Date: 25/06/2021 10:10:10:Am ******/
CREATE USER [topaso8107_SQLLogin_1] FOR LOGIN [topaso8107_SQLLogin_1] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [topaso8107_SQLLogin_1]
GO
/****** Object:  Schema [topaso8107_SQLLogin_1]    Script Date: 25/06/2021 10:10:10:Am ******/
CREATE SCHEMA [topaso8107_SQLLogin_1]
GO
/****** Object:  Table [dbo].[departamento]    Script Date: 25/06/2021 10:10:10:Am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[departamento](
	[iddepartamento] [int] IDENTITY(1,1) NOT NULL,
	[departamento] [varchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[iddepartamento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[descuentos]    Script Date: 25/06/2021 10:10:10:Am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[descuentos](
	[iddescuentos] [int] IDENTITY(1,1) NOT NULL,
	[idtipodesc] [int] NOT NULL,
	[cantidad] [decimal](12, 2) NOT NULL,
	[descuento] [decimal](12, 2) NOT NULL,
	[idempleado] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[iddescuentos] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[detalleplanilla]    Script Date: 25/06/2021 10:10:10:Am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[detalleplanilla](
	[iddetalleplanilla] [int] IDENTITY(1,1) NOT NULL,
	[idplanilla] [int] NOT NULL,
	[idempleados] [int] NOT NULL,
	[iddescuentos] [int] NOT NULL,
	[horasextradiu] [int] NOT NULL,
	[horasextranoc] [int] NOT NULL,
	[bonos] [decimal](12, 2) NOT NULL,
	[horasdiu] [int] NOT NULL,
	[horasnoc] [int] NOT NULL,
	[isss] [decimal](12, 2) NOT NULL,
	[afp] [decimal](12, 2) NOT NULL,
	[estadorenta] [varchar](25) NOT NULL,
	[renta] [decimal](12, 2) NOT NULL,
	[salarioneto] [decimal](12, 2) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[iddetalleplanilla] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[empleado]    Script Date: 25/06/2021 10:10:10:Am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[empleado](
	[idempleado] [int] IDENTITY(1,1) NOT NULL,
	[codempleado] [int] NOT NULL,
	[nombre] [varchar](150) NOT NULL,
	[apellido] [varchar](100) NOT NULL,
	[direccion] [varchar](250) NOT NULL,
	[telefono] [varchar](50) NOT NULL,
	[dui] [varchar](25) NOT NULL,
	[nit] [varchar](50) NOT NULL,
	[enfermedad] [varchar](150) NOT NULL,
	[fechaingreso] [datetime] NOT NULL,
	[sueldo] [decimal](12, 2) NOT NULL,
	[idtipopago] [int] NOT NULL,
	[iddepartamento] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[idempleado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[plandesc]    Script Date: 25/06/2021 10:10:10:Am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[plandesc](
	[idplanilladescuento] [int] IDENTITY(1,1) NOT NULL,
	[iddetalleplanilla] [int] NOT NULL,
	[iddescuento] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[idplanilladescuento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[planilla]    Script Date: 25/06/2021 10:10:10:Am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[planilla](
	[idplanilla] [int] IDENTITY(1,1) NOT NULL,
	[mes] [varchar](20) NOT NULL,
	[parte] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[idplanilla] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[roles]    Script Date: 25/06/2021 10:10:10:Am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[roles](
	[idrol] [int] IDENTITY(1,1) NOT NULL,
	[rol] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[idrol] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tipodesc]    Script Date: 25/06/2021 10:10:10:Am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tipodesc](
	[idtipodesc] [int] IDENTITY(1,1) NOT NULL,
	[tipodesc] [varchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[idtipodesc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tipopagos]    Script Date: 25/06/2021 10:10:10:Am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tipopagos](
	[idtipopago] [int] IDENTITY(1,1) NOT NULL,
	[tipopago] [varchar](150) NULL,
PRIMARY KEY CLUSTERED 
(
	[idtipopago] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[usuario]    Script Date: 25/06/2021 10:10:10:Am ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[usuario](
	[idusuario] [int] IDENTITY(1,1) NOT NULL,
	[usuario] [varchar](50) NOT NULL,
	[contrasena] [varchar](150) NOT NULL,
	[idrol] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[idusuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[descuentos]  WITH CHECK ADD  CONSTRAINT [idempleado] FOREIGN KEY([idempleado])
REFERENCES [dbo].[empleado] ([idempleado])
GO
ALTER TABLE [dbo].[descuentos] CHECK CONSTRAINT [idempleado]
GO
ALTER TABLE [dbo].[descuentos]  WITH CHECK ADD  CONSTRAINT [idtipodesc] FOREIGN KEY([idtipodesc])
REFERENCES [dbo].[tipodesc] ([idtipodesc])
GO
ALTER TABLE [dbo].[descuentos] CHECK CONSTRAINT [idtipodesc]
GO
ALTER TABLE [dbo].[detalleplanilla]  WITH CHECK ADD  CONSTRAINT [iddescuentos] FOREIGN KEY([iddescuentos])
REFERENCES [dbo].[descuentos] ([iddescuentos])
GO
ALTER TABLE [dbo].[detalleplanilla] CHECK CONSTRAINT [iddescuentos]
GO
ALTER TABLE [dbo].[detalleplanilla]  WITH CHECK ADD  CONSTRAINT [idempleados] FOREIGN KEY([idempleados])
REFERENCES [dbo].[empleado] ([idempleado])
GO
ALTER TABLE [dbo].[detalleplanilla] CHECK CONSTRAINT [idempleados]
GO
ALTER TABLE [dbo].[detalleplanilla]  WITH CHECK ADD  CONSTRAINT [idplanilla] FOREIGN KEY([idplanilla])
REFERENCES [dbo].[planilla] ([idplanilla])
GO
ALTER TABLE [dbo].[detalleplanilla] CHECK CONSTRAINT [idplanilla]
GO
ALTER TABLE [dbo].[empleado]  WITH CHECK ADD  CONSTRAINT [iddepartamento] FOREIGN KEY([iddepartamento])
REFERENCES [dbo].[departamento] ([iddepartamento])
GO
ALTER TABLE [dbo].[empleado] CHECK CONSTRAINT [iddepartamento]
GO
ALTER TABLE [dbo].[empleado]  WITH CHECK ADD  CONSTRAINT [idtipopago] FOREIGN KEY([idtipopago])
REFERENCES [dbo].[tipopagos] ([idtipopago])
GO
ALTER TABLE [dbo].[empleado] CHECK CONSTRAINT [idtipopago]
GO
ALTER TABLE [dbo].[plandesc]  WITH CHECK ADD  CONSTRAINT [iddescuento] FOREIGN KEY([iddescuento])
REFERENCES [dbo].[descuentos] ([iddescuentos])
GO
ALTER TABLE [dbo].[plandesc] CHECK CONSTRAINT [iddescuento]
GO
ALTER TABLE [dbo].[plandesc]  WITH CHECK ADD  CONSTRAINT [iddetalleplanilla] FOREIGN KEY([iddetalleplanilla])
REFERENCES [dbo].[detalleplanilla] ([iddetalleplanilla])
GO
ALTER TABLE [dbo].[plandesc] CHECK CONSTRAINT [iddetalleplanilla]
GO
ALTER TABLE [dbo].[usuario]  WITH CHECK ADD  CONSTRAINT [idrol] FOREIGN KEY([idrol])
REFERENCES [dbo].[roles] ([idrol])
GO
ALTER TABLE [dbo].[usuario] CHECK CONSTRAINT [idrol]
GO
USE [master]
GO
ALTER DATABASE [Planilla] SET  READ_WRITE 
GO
